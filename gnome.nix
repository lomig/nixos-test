{ config, pkgs, ... }:

{
 services.xserver.desktopManager.gnome3.enable = true ;
  environment.systemPackages = with pkgs; [
    gnome3.gnome-tweaks
    gnome3.gnome-boxes
  ];

  services.gnome3.core-utilities.enable = false ;
  services.gnome3.gnome-keyring.enable = true;
  programs.gnome-terminal.enable = true ;
  programs.evince.enable = true ;
  programs.file-roller.enable = true ;

}
